const initialState = {
  loginData: {
    email: '',
    role: '',
    id: '',
    token: '',
    branchid: '',
    branch: '',
    nrp: '',
  },
  isLoggedIn: false,
};

const loginReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_LOGIN':
      return {
        ...state,
        isLoggedIn: true,
        loginData: action.data,
      };
    case 'RESET_DATA_LOGIN':
      return {
        ...state,
        loginData: {
          nama: '',
          email: '',
          phoneNumber: '',
          address: '',
          imgProfile: '',
          password: '',
        },
        isLoggedIn: false,
      };
    case 'LOGIN':
      return {
        ...state,

        isLoggedIn: true,
      };
    case 'LOGOUT':
      return {
        ...state,
        isLoggedIn: false,
      };

    default:
      return state;
  }
};

export default loginReducers;
