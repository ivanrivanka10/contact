import {ActivityIndicator, View} from 'react-native';
import React from 'react';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export const WaitingLoading = () => {
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: Colors.WHITE,
      }}>
      <ActivityIndicator size="large" color={Colors.PRIMARY} />
    </View>
  );
};
