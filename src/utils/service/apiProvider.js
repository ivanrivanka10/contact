import axios from 'axios';
import {BASE_URL, TOKEN} from './url';

const API = async (
  url,
  options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'PATCH' ||
    request.method === 'DELETE'
  )
    request.data = options.body;

  const res = await axios(request);

  if (res.status === 200) {
    return res.data;
  } else {
    return res;
  }
};

export default {
  getDataContact: async () => {
    return API(`contact`, {
      method: 'GET',
      head: {'Content-Type': 'application/json'},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  getDataDetailContact: async params => {
    return API(`contact/${params}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json'},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  DeleteDataContact: async (id, params) => {
    return API(`contact/${id}`, {
      method: 'DELETE',
      head: {'Content-Type': 'application/json'},
      body: params,
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  EditDataContact: async (id, params) => {
    console.log(`contact/${id}`);
    return API(`contact/${id}`, {
      method: 'PUT',
      head: {'Content-Type': 'application/json'},
      body: params,
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
};
