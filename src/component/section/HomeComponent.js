import React, {useEffect, useState} from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {Colors} from '../../styles';
import {TextBold, TextRegular} from '../global';
import {ScrollView} from 'react-native-gesture-handler';
import ModalAddData from '../modal/ModalAddData';

const HomeComponent = ({navigation, listContact, onDelete}) => {
  const [sortedList, setSortedList] = useState([]);
  const [modalBottom, setModalBottom] = useState(false);
  useEffect(() => {
    // Mengurutkan array listContact berdasarkan nama kontak
    const sortedArray = [...listContact].sort((a, b) => {
      const nameA = `${a.firstName} ${a.lastName}`.toUpperCase();
      const nameB = `${b.firstName} ${b.lastName}`.toUpperCase();
      if (nameA < nameB) return -1;
      if (nameA > nameB) return 1;
      return 0;
    });

    setSortedList(sortedArray);
  }, [listContact]);

  const RenderItem = ({item}) => (
    <TouchableOpacity
      style={styles.contactItem}
      onPress={() => navigation.navigate('DetailContact', item)}>
      <Image
        source={{
          uri: item.photo,
        }}
        style={styles.avatar}
      />
      <View style={styles.contactInfo}>
        <View
          style={{
            flexDirection: 'row',
            height: '100%',
            alignItems: 'center',
          }}>
          <TextBold
            text={item.firstName}
            style={{marginRight: 5}}
            color={Colors.PRIMARY}
          />
          <TextBold text={item.lastName} color={Colors.PRIMARY} />
        </View>
        <View
          style={{flexDirection: 'row', height: '100%', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              onDelete(item);
            }}>
            <View
              style={{
                backgroundColor: Colors.RED,
                height: 40,
                width: 40,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 40,
              }}>
              <Image
                source={require('../../asset/delete.png')}
                style={{
                  width: 20,
                  height: 20,
                  tintColor: Colors.WHITE,
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('EditContact', item);
            }}>
            <View
              style={{
                backgroundColor: Colors.PRIMARY,
                height: 40,
                width: 40,
                marginLeft: 4,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 40,
              }}>
              <Image
                source={require('../../asset/edit.png')}
                style={{
                  width: 20,
                  height: 20,
                  tintColor: Colors.WHITE,
                  marginLeft: 4,
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={{backgroundColor: Colors.BACKGROUND, flex: 1}}>
      <ScrollView style={{backgroundColor: Colors.BACKGROUND, flex: 1}}>
        <ImageBackground
          source={require('../../asset/wallpaper.jpg')}
          resizeMode="cover"
          style={{
            flex: 1,
            paddingTop: 20,
            height: 200,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
            }}>
            <TextBold text={'Contact ('} color={Colors.WHITE} />
            <TextBold text={listContact.length} color={Colors.WHITE} />
            <TextBold text={')'} color={Colors.WHITE} />
          </View>
        </ImageBackground>
        <View
          style={{
            width: '95%',
            alignSelf: 'center',
            marginTop: -140,
            backgroundColor: Colors.WHITE,
            borderRadius: 20,
          }}>
          <FlatList
            data={sortedList}
            renderItem={({item}) => <RenderItem item={item} />} // Update this line
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        onPress={() => setModalBottom(true)}
        style={{
          width: 50,
          height: 50,
          backgroundColor: Colors.GREEN,
          position: 'absolute',
          borderRadius: 50,
          right: '3%',
          bottom: '3%',
          alignContent: 'center',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          source={require('../../asset/addprofile.png')}
          style={{
            width: 20,
            height: 20,
            tintColor: Colors.WHITE,
            marginLeft: 4,
          }}
        />
      </TouchableOpacity>
      <ModalAddData
        show={modalBottom}
        onClose={() => setModalBottom(false)}></ModalAddData>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    marginBottom: 4,
    padding: 10,
    borderRadius: 20,
    backgroundColor: Colors.WHITE,
    borderBottomWidth: 1,
    borderColor: Colors.PRIMARY,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
    borderWidth: 2,
    borderColor: Colors.PRIMARY,
  },
  contactInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
  },
});
export default HomeComponent;
