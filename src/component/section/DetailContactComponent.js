import React from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {Colors} from '../../styles';
import {TextBold, TextRegular} from '../global';
import {ScrollView} from 'react-native-gesture-handler';

const DetailContactComponent = ({navigation, dataDetail}) => {
  return (
    <View style={{flex: 1, backgroundColor: Colors.BACKGROUND2}}>
      <ScrollView>
        <ImageBackground
          source={require('../../asset/wallpaper.jpg')}
          resizeMode="cover"
          style={{
            flex: 1,
            paddingTop: 20,
            paddingBottom: 90,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '95%',
            }}>
            <TouchableOpacity onPress={() => navigation.pop()}>
              <Image
                source={require('../../asset/back.png')}
                style={{
                  width: 30,
                  height: 30,
                  alignSelf: 'center',
                  borderRadius: 300,
                  tintColor: Colors.WHITE,
                  alignSelf: 'flex-start',
                }}
              />
            </TouchableOpacity>
            <TextBold text={'Detail Contact'} color={Colors.WHITE} size={20} />

            <View style={{width: 30, height: 30}}></View>
          </View>

          <Image
            source={{uri: dataDetail.photo}}
            style={{
              width: 100,
              height: 100,

              marginTop: 21,
              borderRadius: 300,
              borderWidth: 1,
              borderColor: Colors.WHITE,
            }}
          />
        </ImageBackground>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 10,
          }}>
          <TextBold text={'First Name'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={dataDetail.firstName}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 10,
          }}>
          <TextBold text={'Last Name'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={dataDetail.lastName}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 2,
            marginTop: 10,
          }}>
          <TextBold text={'Age'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={dataDetail.age}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 2,
            marginTop: 10,
          }}>
          <TextBold text={'Phone Number'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={'0123456789'}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 10,
          }}>
          <TextBold text={'Email'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={'NextUpdate@gmail.com'}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 10,
          }}>
          <TextBold text={'Address'} size={20} color={Colors.BACKGROUND} />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <TextRegular
            text={'Dummy Address For Next Update'}
            size={20}
            color={Colors.BACKGROUND}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
  },
  contactInfo: {
    flex: 1,
    flexDirection: 'row',
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  email: {
    fontSize: 14,
    color: '#888',
  },
});
export default DetailContactComponent;
