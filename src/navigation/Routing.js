import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {NavigationContainer} from '@react-navigation/native';
import Home from '../screen/Contact/Home';
import DetailContact from '../screen/Contact/DetailContact';
import EditContact from '../screen/Contact/EditContact';
const Stack = createStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, tabBarHideOnKeyboard: true}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="DetailContact" component={DetailContact} />
        <Stack.Screen name="EditContact" component={EditContact} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
